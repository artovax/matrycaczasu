package com.example.artovax.zarzadzanie_czasem;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by Artovax on 2015-05-10.
 */
public class SzczegolyNotatkiFragment extends Fragment {
    public static final String NOTATKA_ARG = "notatka.nazwa";

    private EditText mNotatkaSzczegoly;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.szczegoly_notatki_fragment, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNotatkaSzczegoly = (EditText) view.findViewById(R.id.notatka);

        // Przypisujemy nazwe miasta
        String mMiasto = getArguments().getString(NOTATKA_ARG); /// To chwilowo nie krytyczne

        mNotatkaSzczegoly.setText(mMiasto);
    }
}
