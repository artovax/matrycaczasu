package com.example.artovax.zarzadzanie_czasem;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;


public class GlownyWidokActivity extends ActionBarActivity {

    public static final String LISTA_KEY = "Lita napisów";
    private static final String TAG = GlownyWidokActivity.class.getName();
    private String mPole = "private String mPole :P";


    private Button mButton;
    private LinearLayout mLinear;

    private ArrayList<String> mValues;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glowny_widok);
        mButton = (Button) findViewById(R.id.przycisk);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                przejdzDoNotatki(view);
            }
        });
    }

    public void przejdzDoNotatki(View mView) {
        Intent mIntent = new Intent(this, SzczegolyNotatkiFragment.class);
        //startActivity(mIntent);
        otworzSzczegolyNotatki("");

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_glowny_widok, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void otworzSzczegolyNotatki(String mNazwa){
        //TODO: utworzenie obiektu szczegóły notaki i  Otwarcie fragmentu szczegolow


        SzczegolyNotatkiFragment mFragment = new SzczegolyNotatkiFragment();
        Bundle mArguments = new Bundle();

        mArguments.putString(SzczegolyNotatkiFragment.NOTATKA_ARG, mNazwa);
        mFragment.setArguments(mArguments);

        // Otwarcie fragmentu szczegolow
        getSupportFragmentManager().
                beginTransaction().
                add(R.id.content, mFragment, "details").
                addToBackStack(null).
                commit();

    }
}

